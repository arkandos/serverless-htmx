import { resolve } from 'path'
import { defineConfig } from 'vite'

// TODO: figure out dev builds...
export default defineConfig({
    build: {
        rollupOptions: {
            input: {
                main: resolve(__dirname, 'index.html'),
                sw: resolve(__dirname, 'src/index.tsx')
            },
            output: {
                entryFileNames (chunk) {
                    if (chunk.name === 'sw') {
                        return 'sw.js'
                    } else {
                        return 'assets/[name]-[hash].js'
                    }
                }
            }
        },
    },
    resolve: {
        alias: {
            '@': resolve(__dirname, './src/')
        }
    },

})
