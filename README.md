# HTMX without a Server

Usually, HTMX assumes a server. This is a demo of using a Service Worker instead. The Service Worker takes on the role of the server, responding to `fetch` events with custom logic. Instead of a real server though this runs entirely in the users browser.

For this demo, I shamelessly use [my own routing library](https://www.npmjs.com/package/@jreusch/router-node?activeTab=readme) since it's the only one I know that can be plugged into anything, and [preact-render-to-string](https://preactjs.com/guide/v10/server-side-rendering/) as a templating engine. You can probably figure out how to replace both of these with your own stuff.

## Running

I haven't figured out how to to the service worker properly within DEV mode. I tried `vite-plugin-native-sw` which worked in dev, but threw some Typescript errors when trying to build things.

For now, do the following:

```bash
npm install
npm run build
npm run preview
```
