import { h } from 'preact'
import { render } from 'preact-render-to-string'

import { get, post, compile } from '@jreusch/router-node'

import { Index, Count } from './pages/Index'

/// <reference lib="webworker" />
export type {}

declare const self: ServiceWorkerGlobalScope

self.addEventListener('error', console.error)

// these 2 event listeners make sure the browser replaces the server worker
// as soon as possible if it detects an update.
self.addEventListener('install', event => {
    event.waitUntil(self.skipWaiting())
})

self.addEventListener('activate', event => {
    event.waitUntil(self.clients.claim())
})


// state - we use a local variable here, which roughly translates to a session variable.
// IndexedDB is also available in service workers if you need to persist stuff.
let count = 0

// router. see @jreusch/router-node
const router = compile(
    // how to get the method/pathname
    (req: Request) => req.method,
    (req: Request) => new URL(req.url).pathname,

    // actual routes. we return JSX fragments here.
    get('/', (params, req) => <Index count={count} />),
    post('/increment', (params, req) => <Count count={++count} />),
    post('/decrement', (params, req) => <Count count={--count} />),
)

// act as a "server" by handling fetch events
self.addEventListener('fetch', event => {
    const url = new URL(event.request.url)
    // if you request a 3rd-party page, always do the default
    if (url.hostname !== location.hostname) {
        return
    }

    // call the router to get the vdom
    const vdom = router(event.request)
    // if we get null, we could not find the route, so do the browser default.
    // (this might happen when requesting js/css/fonts etc)
    if (!vdom) {
        return
    }

    // client-side server-side-rendering...
    // have we gone to far??
    const html = '<!DOCTYPE html>\n' + render(vdom)
    const response = new Response(html, {
        headers: {
            'Content-Type': 'text/html'
        }
    })

    event.respondWith(response)
})
