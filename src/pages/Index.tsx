import { h } from 'preact'
import Page from './Page'

export function Index(props: { count: number }) {
    return <Page title="Serverless HTMX">
        <h1>HTMX without a Server!</h1>

        <p>
            <button hx-post="/decrement" hx-target="#count" hx-swap="outerHTML">
                -
            </button>

            <Count count={props.count} />

            <button hx-post="/increment" hx-target="#count" hx-swap="outerHTML">
                +
            </button>
        </p>

        <h2>What is this?</h2>
        <p>
            Usually, HTMX assumes a server. This is a demo of using a Service Worker instead. The Service Worker takes on the role of the server, responding to <code>fetch</code> events with custom logic. Instead of a real server though this runs entirely in the users browser.
        </p>

        <p>
            For this demo, I shamelessly use <a href="https://www.npmjs.com/package/@jreusch/router-node?activeTab=readme">my own routing library</a> since it's the only one I know that can be plugged into anything, and <a href="https://preactjs.com/guide/v10/server-side-rendering/">preact-render-to-string</a> as a templating engine. You can probably figure out how to replace both of these with your own stuff.
        </p>

        <p>
            You can look at the source code of this page on <a href="https://gitlab.com/arkandos/serverless-htmx">Gitlab</a>. Use it for whatever you want!
        </p>

    </Page>
}

// a separate template for the Count piece that we can render separately and swap in using htmx
export function Count(props: { count: number }) {
    return <span id="count">
        Clicked {props.count} times
    </span>
}
